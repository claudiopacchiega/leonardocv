#!/usr/bin/env python
# coding: utf-8

# In[1]:


#tratto da https://sites.temple.edu/tudsc/2017/03/30/measuring-similarity-between-texts-in-python/

d1 = "plot: two teen couples go to a church party, drink and then drive."
d2 = "films adapted from comic books have had plenty of success , whether they're about superheroes ( batman , superman , spawn ) , or geared toward kids ( casper ) or the arthouse crowd ( ghost world ) , but there's never really been a comic book like from hell before . "
d3 = "every now and then a movie comes along from a suspect studio , with every indication that it will be a stinker , and to everybody's surprise ( perhaps even the studio ) the film becomes a critical darling . "
d4 = "damn that y2k bug . "
documents = [d1, d2, d3, d4]


# In[73]:


from tika import parser
import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.tokenize import word_tokenize

import glob
import string
from nltk.corpus import stopwords
stopwordsit = stopwords.words('italian')
stopwordsen = stopwords.words('english')
files = glob.glob("../doc/*")
files.sort()

def process(file):
    parsedPDF = parser.from_file(file)
    text = parsedPDF['content'].lower()
    text_tokens = word_tokenize(text)
    tokens_without_sw = [word for word in text_tokens if not (word in stopwordsit or word in stopwordsen or word in string.punctuation or len(word)<3)]
    text= " ".join(tokens_without_sw)
    return text


d1 = "java trama: una coppia di adolescenti va in una festa in una chiesa, si ubriacano e quindi guidano."
d2 = "film html adattati dai fumetti hanno molto successo, sia che parlino di supereroi ( batman , superman , spawn ) , o che siano indirizzati ai bambini ( casper ) o film d'autore per le masse ( mondo spettrale ) , ma non c'e' mai stato un fumetto come dall'inferno prima . "
d3 = "di tanto in html tanto esce un film prodotto da uno studio sospetto , con ogni indicazione che sara' un fallimento , e sorprendendo tutti  ( perfino lo studio che lo ha prodotto ) il film diventa un campione di incassi . "
d4 = "maledizione a quel baco del millennio . java trama"




documents = [process(f) for f in files]


# In[76]:


documents[70]


# In[37]:


import nltk, string, numpy
from nltk.stem.snowball import SnowballStemmer
nltk.download('punkt') # first-time use only
stemmer = SnowballStemmer('italian')
def StemTokens(tokens):
    return [stemmer.stem(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
def StemNormalize(text):
    return StemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


# In[30]:


StemNormalize("alle falde del kilimangiaro ci stanno i watussi")


# In[48]:


nltk.download('wordnet') # first-time use only

lemmer = nltk.stem.WordNetLemmatizer()
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


# In[77]:


from sklearn.feature_extraction.text import CountVectorizer

#list = stopwords.words('italian')
LemVectorizer = CountVectorizer(tokenizer=LemNormalize) #, stop_words=list)
LemVectorizer.fit_transform(documents)


# In[78]:


print (LemVectorizer.vocabulary_)
tf_matrix = LemVectorizer.transform(documents).toarray()
print (tf_matrix)
tf_matrix.shape


# In[79]:


from sklearn.feature_extraction.text import TfidfTransformer
tfidfTran = TfidfTransformer(norm="l2")
tfidfTran.fit(tf_matrix)
print (tfidfTran.idf_)


# In[16]:


import math
def idf(n,df):
    result = math.log((n+1.0)/(df+1.0)) + 1
    return result
print ("The idf for terms that appear in one document: " + str(idf(4,1)))
print ("The idf for terms that appear in two documents: " + str(idf(4,2)))


# In[80]:


tfidf_matrix = tfidfTran.transform(tf_matrix)
print (tfidf_matrix.toarray())
print(tfidf_matrix.shape)


# In[81]:


cos_similarity_matrix = (tfidf_matrix * tfidf_matrix.T).toarray()
print (cos_similarity_matrix)
min = 0.3
for i in range(len(cos_similarity_matrix)):
    for j in range(i):
        if not(i == j):
            if cos_similarity_matrix[i,j] >= min:
                print("["+str(i)+","+str(j)+"] have cosine similarity "+str(cos_similarity_matrix[i,j]))


print("ok")