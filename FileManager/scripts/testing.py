import nltk


from nltk.corpus import stopwords
from tika import parser

dir = '/home/pakkio/w/leonardocv/FileManager/doc'
import math
from textblob import TextBlob as tb
stopset = stopwords.words('italian')

def loadWords(name):
    raw = parser.from_file('%s/%s' % (dir,name))
    doc1 = (raw['content'].strip().lower())
    mytb = tb(doc1)
    filtered_sentence = [w for w in mytb.words if not w in stopset]
    return tb(" ".join(filtered_sentence))

from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(dir) if isfile(join(dir, f))]
bloblist = [loadWords(f) for f in onlyfiles]

import math
from textblob import TextBlob as tb

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob.words)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

# def tf(word, blob):
#     return (float)(blob.words.count(word)) / (float)(len(blob.words))
#
# def n_containing(word, bloblist):
#     return (float)(sum(1 for blob in bloblist if word in blob))
#
# def idf(word, bloblist):
#     return (float)(math.log(len(bloblist)) / (float)(1 + n_containing(word, bloblist)))
#
# def tfidf(word, blob, bloblist):
#     return (float)((float)(tf(word, blob)) * (float)(idf(word, bloblist)))

for i, blob in enumerate(bloblist):
    print("Top words in document {}".format(i + 1))
    scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)

    for word, score in sorted_words[:10]:
        print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))
