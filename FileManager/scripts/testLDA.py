# all the imports

from tika import parser

import nltk
from nltk.corpus import stopwords
import os
cwd = os.getcwd()
from nltk.tokenize import word_tokenize

import turicreate as tc

from os import listdir
from os.path import isfile, join
import glob

nltk.download('stopwords')
nltk.download('punkt')

# gets the file to parse all pdf at the moment
files = glob.glob("../doc/*")
files.sort()
#files.append(glob.glob("/home/pakkio/w/untitled/Leonardo - CV/cv/Esempi CV Big Data/Altri/*.pdf"))
print(len(files))

stopwordsit = stopwords.words('italian')
stopwordsen = stopwords.words('english')
punctuation = ['.',',','(',')','[',']','?','!','+','-','@',':',';']

# generate a text removing all stop words in italian since turicreate is not doing for this language
i=0
ids=[]
fnames=[]
texts=[]


for file in files:
    i+=1
    parsedPDF = parser.from_file(file)
    text = parsedPDF['content'].lower()

    text_tokens = word_tokenize(text)
    tokens_without_sw = [word for word in text_tokens if not (word in stopwordsit or word in stopwordsen or word in punctuation or len(word)<3)]
    text= " ".join(tokens_without_sw)
    ids.append(i)
    fnames.append(file)
    texts.append(text)


sf = tc.SFrame({'id':ids,'file':fnames,'text':texts})
sf.save("cv1.sframe")
sf['text']

# now we should apply the LDA algorithm
corpus = tc.SFrame("cv1.sframe")
docs = tc.text_analytics.count_words(corpus['text'])
docs = docs.dict_trim_by_values(2)

associations = tc.SFrame({'word':['java', 'perl', 'javascript', 'unix', 'linux', 'inglese', 'certificate', 'B1'],
                          'topic': [1, 2, 3, 4, 4, 5, 5, 8]})

topic_model = tc.topic_model.create(docs, num_topics=6, num_iterations=1000, associations = associations, verbose = False)
docs_tfidf = tc.text_analytics.tf_idf(corpus['text'])
i=0
for d in docs_tfidf:
    i+=1
    print("\ndocument ",i, corpus[i-1]['file'])
    for w in sorted(d, key=d.get, reverse=True):
        if(d[w]> 20):
            print (w,d[w])

topics = topic_model.get_topics()
argomenti = [x['words'] for x in topic_model.get_topics(output_type='topic_words')]
argomenti

