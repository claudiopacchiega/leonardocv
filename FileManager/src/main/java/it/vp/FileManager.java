package it.vp;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.*;

public final class FileManager {

    public static String detectLanguage(String text) throws IOException {
        LanguageDetector detector = new OptimaizeLangDetector().loadModels();
        LanguageResult result = detector.detect(text);
        return result.getLanguage();
    }

    public static String detect(File file) throws IOException {
        DefaultDetector detector = new DefaultDetector();

        Metadata metadata = new Metadata();
        metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
        MediaType mediaType = null;
        mediaType = detector.detect(new BufferedInputStream(new FileInputStream(file)), metadata);
        return mediaType.getSubtype();
    }

    public static String parse(File file) throws IOException, TikaException, SAXException {
        AutoDetectParser parser = new AutoDetectParser();
        BodyContentHandler handler = new BodyContentHandler(-1);
        Metadata metadata = new Metadata();
        ParseContext parseContext = new ParseContext();
        PDFParserConfig pdfConfig = new PDFParserConfig();
        pdfConfig.setEnableAutoSpace(false);
        //Sort text tokens by their x/y position before extracting text
        //pdfConfig.setSortByPosition(true);
        parseContext.set(PDFParserConfig.class, pdfConfig);
        InputStream stream = null;
        stream = new FileInputStream(file);
        parser.parse(stream, handler, metadata);
        return handler.toString();
    }
}
