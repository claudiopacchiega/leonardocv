import it.vp.FileManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import java.io.*;
public class TestMain {
    static Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        File[] myListOfFiles = new File("doc").listFiles();
        String filename = null;
        String delimiter = "|";
        String regexToReplaceWithWhiteSpace = "\n|\r|\t|\\" + delimiter;
        try (PrintWriter out = new PrintWriter("output/outputCV.csv","UTF-8")) {
            String header = "FILENAME" + delimiter + "FILE_TYPE" + delimiter + "TEXT_LANGUAGE" + delimiter + "TEXT";
            out.println(header);
            for (File f : myListOfFiles) {
                filename = f.getName();
                String type = FileManager.detect(f);
                String data = FileManager.parse(f);
                String language = FileManager.detectLanguage(data);
                String dataWithoutNewLine = data.replaceAll(regexToReplaceWithWhiteSpace, " ");
                String outputString = f.getName() + delimiter + type + delimiter + language + delimiter + dataWithoutNewLine;
                out.println(outputString);
            }
        } catch (IOException | TikaException | SAXException e) {
            logger.error("Filename "+ filename +" with error: " + e);
        }
    }
}
//http://aircconline.com/ijaia/V10N1/10119ijaia02.pdf
/*FOR READING PDF
PDDocument document = PDDocument.load(f);
if (!document.isEncrypted()) {
PDFTextStripper stripper = new PDFTextStripper();
String text = stripper.getText(document);
System.out.println("Text:" + text);
}
document.close();*/