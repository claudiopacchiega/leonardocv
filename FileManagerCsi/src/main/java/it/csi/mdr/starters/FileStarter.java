package it.csi.mdr.starters;

import it.csi.mdr.configuration.ReadConfig;
import it.csi.mdr.constants.InputConstants;
import it.csi.mdr.managers.FileManager;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;


public class FileStarter {
    static Logger logger = Logger.getLogger(FileStarter.class);
    public static void main(String[] args) {
        //Get input parameters
        HashMap<String, String> inputParameters = ReadConfig.getInstance().getParameters();
        String endpointSolr = inputParameters.get(InputConstants.ENDPOINT_SOLR);
        String collectionName = inputParameters.get(InputConstants.COLLECTION_NAME);
        String documentsRootPath = inputParameters.get(InputConstants.DOCUMENTS_ROOT_PATH);
        String regexFileFilter = inputParameters.getOrDefault(InputConstants.REGEX_FILE_FILTER, ".*"); //ex. in conf file: regexFileFilter=(.*\\/18[0-9]+.*\\.txt|.*\\/(19|20)[0-9]+.*\\.pdf)

        AtomicInteger numberOfFiles = new AtomicInteger(0);
        AtomicInteger numberOfFilesError = new AtomicInteger(0);
        try {
            //Define solr ConcurrentUpdateSolrClient connection (recommended only for /update requests)
            SolrClient client = new ConcurrentUpdateSolrClient.Builder(endpointSolr).build();
            //For each file in the root path
            Files.walk(Paths.get(documentsRootPath))
                    .filter(path -> Files.isRegularFile(path) && path.toString().matches(regexFileFilter))
                    .forEach(path -> {
                        //Define new solr document
                        SolrInputDocument document = new SolrInputDocument();
                        File file = path.toFile();
                        logger.info("Working with file: " + file);
                        try {
                            //Get meta and data from file
                            FileManager fm = new FileManager(file);
                            Metadata meta = fm.getMeta();
                            String data = fm.getData();
                            //Insert data into document
                            document.addField("object_id", path.toString());
                            document.addField("doc_text", data);
                            for(String metaName : meta.names()) {
                                document.addField("meta_" + metaName, meta.get(metaName));
                            }
                            //Add document into collection
                            client.add(collectionName, document);
                        } catch (IOException | TikaException | SAXException | SolrServerException e) {
                            numberOfFilesError.addAndGet(1);
                            logger.error(e);
                        } finally {
                            numberOfFiles.addAndGet(1);
                        }
                    });
            //Commit on collection
            logger.info("Commit on collection: " + collectionName);
            client.commit(collectionName);
            //Log info count
            logger.info("Number of files committed: " + numberOfFiles);
            logger.info("Number of files in error: " + numberOfFilesError);
            //Close connection
            client.close();
        } catch (IOException e) {
            logger.error(e);
        } catch (SolrServerException e) {
            logger.error(e);
        }
    }
}