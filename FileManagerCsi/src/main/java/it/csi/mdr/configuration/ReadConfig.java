package it.csi.mdr.configuration;

import it.csi.mdr.constants.InputConstants;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Properties;

//Singleton class
public class ReadConfig {
    static Logger logger = Logger.getLogger(ReadConfig.class);
    // static variable single_instance of type ReadConfig
    private static ReadConfig single_instance = null;

    // Hashmap used to map (key, value) parameters
    private static HashMap<String, String> parameters = new HashMap<String, String>();

    //Private constructor used in singleton class
    private ReadConfig() {
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            logger.info("Read property file");
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Property file '" + propFileName + "' not found in the classpath");
            }

            /*Adding (key, value)) to HashMap*/
            for(String key : prop.stringPropertyNames()) {
                parameters.put(key, prop.getProperty(key));
            }
            /*Look for parameters*/
            for(Field inputObject : InputConstants.class.getFields()){
                if(parameters.containsKey(inputObject.get(inputObject))){
                    logger.info("Find parameter: " + inputObject.get(inputObject));
                }
                else {
                    logger.warn("No found parameter: " + inputObject.get(inputObject));
                }
            }

        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        } catch (IllegalAccessException e) {
            logger.error(e);
        }
    }

    //Static method to create instance of Singleton class
    public static ReadConfig getInstance()
    {
        if (single_instance == null)
            single_instance = new ReadConfig();

        return single_instance;
    }

    public static HashMap<String, String> getParameters() {
        return parameters;
    }
}
