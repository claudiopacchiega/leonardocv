package it.csi.mdr.managers;

import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class FileManager {

    private String data;
    private Metadata meta;

    private FileManager(){}
    public FileManager(File f) throws IOException, TikaException, SAXException {
        //Define AutoDetectParser
        AutoDetectParser parser = new AutoDetectParser();
        BodyContentHandler handler = new BodyContentHandler(-1);
        Metadata metadata = new Metadata();
        //Set pdf config to remove extra space characters
        ParseContext parseContext = new ParseContext();
        PDFParserConfig pdfConfig = new PDFParserConfig();
        pdfConfig.setEnableAutoSpace(false);
        //Sort text tokens by their x/y position before extracting text
        //pdfConfig.setSortByPosition(true);
        parseContext.set(PDFParserConfig.class, pdfConfig);
        //Parse file stream
        InputStream stream = new FileInputStream(f);
        parser.parse(stream, handler, metadata, parseContext);
        //Get data and metadata
        this.data = handler.toString();
        this.meta = metadata;
    }

    public String getData() {
        return data;
    }

    public Metadata getMeta() {
        return meta;
    }
}
