package it.csi.mdr.constants;

public class InputConstants {

    public static final String ENDPOINT_SOLR = "endpointSolr";
    public static final String COLLECTION_NAME = "collectionName";
    public static final String DOCUMENTS_ROOT_PATH = "documentsRootPath";
    public static final String REGEX_FILE_FILTER = "regexFileFilter";

}
